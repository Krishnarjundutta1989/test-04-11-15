//
//  ViewController.h
//  05
//
//  Created by Click Labs134 on 11/4/15.
//  Copyright © 2015 clicklabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "GetDetailsViewController.h"


@interface ViewController : UIViewController<GMSMapViewDelegate,CLLocationManagerDelegate>


@end

