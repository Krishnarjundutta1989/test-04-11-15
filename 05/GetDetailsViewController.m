//
//  GetDetailsViewController.m
//  05
//
//  Created by Click Labs134 on 11/4/15.
//  Copyright © 2015 clicklabs. All rights reserved.
//

#import "GetDetailsViewController.h"

@interface GetDetailsViewController (){
    NSMutableArray *data;
}
@property (strong, nonatomic) IBOutlet UITableView *tblForDetails;

@end

@implementation GetDetailsViewController
@synthesize tblForDetails;
@synthesize dataFromDestination;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    data = [NSMutableArray new];
    
    data = [NSMutableArray arrayWithArray:dataFromDestination];
    tblForDetails.delegate = self;
    
    // Do any additional setup after loading the view.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}


- (NSInteger)tableview:(UITableView *)tableview numberOfRowsInSection:(NSInteger)section
{
    return data.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"reuse"];
    cell.textLabel.text = data[indexPath.row];
    
    return cell;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
