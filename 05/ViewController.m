//
//  ViewController.m
//  05
//
//  Created by Click Labs134 on 11/4/15.
//  Copyright © 2015 clicklabs. All rights reserved.
//

#import "ViewController.h"

@interface ViewController (){
    CLLocationManager *manager;
    CLLocation *currentLocation;
    CLLocationCoordinate2D position;
    NSMutableArray *dataFromDestination;
    NSString *city;
    NSString *state;
    NSString *Country;
    NSMutableArray * arrayForDetails;
    GMSMarker *mark;
    
    
}
@property (strong, nonatomic) IBOutlet GMSMapView *googleMap;

@end

@implementation ViewController
@synthesize googleMap;

- (void)viewDidLoad {
    [super viewDidLoad];
    manager = [[CLLocationManager alloc]init];
    manager.delegate = self;
    manager.desiredAccuracy = kCLLocationAccuracyBest;
    [manager requestWhenInUseAuthorization];
    [manager startUpdatingLocation];
    
    // Do any additional setup after loading the view, typically from a nib.
}
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    //UIAlertView *errorAlert = [[UIAlertView alloc]
    //initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    //[errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    
    currentLocation = newLocation;
    if (currentLocation != 0) {
        position = currentLocation.coordinate;
        mark = [GMSMarker markerWithPosition:currentLocation.coordinate];
        mark.title = @"current loation";
        mark.map = googleMap;
        mark.icon = [GMSMarker markerImageWithColor:[UIColor blueColor]];
        
    }
    
    
    
}
- (void) CilkAndMark  {
    CLGeocoder *geocoder2 = [[CLGeocoder alloc] init];
    
    CLLocation *newLocation2 = [[CLLocation alloc]initWithLatitude:position.latitude
                                                                                        longitude:position.longitude];
    
    [geocoder2 reverseGeocodeLocation:newLocation2
                    completionHandler:^(NSArray *placemarks, NSError *error) {
                        
                        if (error) {
                            NSLog(@"Geocode failed with error: %@", error);
                            return;
                        }
                        
                        if (placemarks && placemarks.count > 0)
                        {
                            CLPlacemark *placemark = placemarks[0];
                            
                            NSDictionary *addressDictionary =
                            placemark.addressDictionary;
                            
                            NSLog(@"%@ ", addressDictionary);
                            if (addressDictionary[@"City"] != NULL ){
                                city = addressDictionary[@"City"];
                                NSLog(@"%@",city);
                                mark = [GMSMarker markerWithPosition:position];
                                mark.title = [NSString stringWithFormat:@"%@",city];
                                
                                mark.map = googleMap;
                                [arrayForDetails addObject:city];
                                
                            }
                            else if ((addressDictionary[@"State"] != NULL )){
                                state = addressDictionary[@"State"];
                                NSLog(@"%@",state);
                                mark = [GMSMarker markerWithPosition:position];
                                mark.title = [NSString stringWithFormat:@"%@",state];
                                
                                mark.map = googleMap;
                                [arrayForDetails addObject:mark];
                            }
                            
                            else if ((addressDictionary[@"Country"] != NULL )){
                                Country = addressDictionary[@"Country"];
                                NSLog(@"%@",Country);
                                mark = [GMSMarker markerWithPosition:position];
                                mark.title = [NSString stringWithFormat:@"%@",Country];
                                
                                mark.map = googleMap;
                                [arrayForDetails addObject:Country];
                            }
                        }
                        
                        
                    }];
    
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender

{
    GetDetailsViewController *detailVC = segue.destinationViewController;
    detailVC.dataFromDestination = arrayForDetails;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
